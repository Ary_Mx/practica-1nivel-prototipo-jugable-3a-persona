﻿using UnityEngine;
using System.Collections;

public class rotation : MonoBehaviour
{



    public Transform PlayerTransform;
    private Vector3 _cameraoffset;
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;
    public bool LookPlayer = false;
    public bool RotateAroundPlayer = true;
    public float RotationSpeed = 5.0f;


    //  public Vector3 distancia;
    void Start()
    {
        _cameraoffset = transform.position - PlayerTransform.position;
    }
    void LateUpdate()
    {
        if (RotateAroundPlayer)
        {
            Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * RotationSpeed, Vector3.up);
            _cameraoffset = camTurnAngle * _cameraoffset;
        }

        Vector3 newPos = PlayerTransform.position + _cameraoffset;
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);


        if (LookPlayer)
            transform.LookAt(PlayerTransform);
    }
}
