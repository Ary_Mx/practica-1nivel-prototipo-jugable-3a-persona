﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private Rigidbody rigidBody;
    private Animator animator;
    private float hInput;
    private float vInput;
    private float runInput;
    private float jumpInput;
    public float currentSpeed = 0;
    public float walkSpeed = 3;
    public float runSpeed = 6;
    public float turnSmoothing = 20f;

    public float idleWaitingTime =5f;
    public float currentWaitingTime =0f;
    // Use this for initialization
    public float velocidad = 0.1f;
    public float salto = 10.0f;
    bool suelo = true;
    bool isjumping = true;
    private float currenttimeinair = 0;

    public float idlestate = 0;
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
        if (rigidBody == null)
        {
            Debug.Log("No rigidbody component found in this gameobject");
            enabled = false;
            return;
        }
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.Log("No animator component found in this gameobject");
            enabled = false;
            return;
        }
    }


    // Update is called once per frame
    Vector3 dir;


    void Update () {
        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");
        runInput = Input.GetAxis("Run");
        jumpInput = Input.GetAxis("Jump");
        handleRotation();
       
       

        if (currentSpeed == 0)
        {
            currentWaitingTime += Time.deltaTime;
        }
        else
        {
            currentWaitingTime = 0;
        }
       
    }
    void jump()
    {

        if (Input.GetButtonDown("Jump"))
        {
            // animator.SetBool("isjumping",isjumping);
            animator.SetTrigger("jump");
            // GetComponent<Rigidbody>().AddForce(new Vector3(0,salto, 0)*velocidad);
            
            
          //  animator.SetBool("touchingground",suelo);
         //   suelo = false;

        }



    }
    void OnTriggerExit(Collider coll)
    {
        
        if (coll.tag == "suelo")
        {

            suelo = false;
            print(suelo);
            animator.SetBool("suelo",suelo);
            print("Exit");
            Physics.gravity = new Vector3(0,-100.0F, 0);
           
        }
    }

    void OnTriggerEnter(Collider coll)
    {

       
        if (coll.tag == "suelo")
        {
            
            suelo = true;
            print(suelo);
            animator.SetBool("suelo", suelo);
            Physics.gravity = new Vector3(0, -9.8F, 0);

        }
    }
    private void OnAnimatorMove()
    {
       // print(currentSpeed);
        animator.SetFloat("Speed",currentSpeed);

       
        if (currentWaitingTime>=idleWaitingTime)
        {
            if (Random.Range(0, 99) < 90)
            {
                idlestate = Random.Range(0,6);
                animator.SetTrigger("IdleWait");
                animator.SetFloat("IdlWaitingvariation", idlestate);

            }
            currentWaitingTime = 0;
        }
      
    }
    private void FixedUpdate()
    {
       jump();
        handleMovement();
       
    }
    private void handleMovement()
    {
        float targetSpeed = walkSpeed;
        if (runInput > 0)
        {
            targetSpeed = runSpeed;
        }
        if (dir.x != 0 || dir.z != 0)
        {
           
           // Vector3 playerMovement = new Vector3(dir.x, 0.0f,dir.z) *targetSpeed * Time.deltaTime;
           transform.Translate((rigidBody.velocity = new Vector3(dir.x, 0, dir.z) * targetSpeed), Space.Self);

        }
 
        else
        {
            rigidBody.velocity = Vector3.zero;
        }
        currentSpeed = rigidBody.velocity.magnitude;
    }
    private void handleRotation()
    {
        if (dir.x != 0 || dir.y != 0)
        {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(dir.x, 0,dir.y));
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnSmoothing);
        }
    }

}
