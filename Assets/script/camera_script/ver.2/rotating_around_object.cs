﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotating_around_object : MonoBehaviour
{


    public Transform player;


    public Vector3 dir;
    public float currentx;
    public float currenty;

    public Vector3 Offset;
    public float sensitivity = 0.2f;

    private Vector3 relative_position;

    void Start()
    {
        //transform.position = player.position + dir;
    }
    void LateUpdate()
    {
        currentx += Offset.x * sensitivity;
        currenty += Offset.y * sensitivity;

        Quaternion rotation = Quaternion.Euler(0, currenty, Offset.z);
        transform.position = player.position - (rotation * Offset)+dir;
        transform.LookAt(player);

        




    }
}
