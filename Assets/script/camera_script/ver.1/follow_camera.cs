﻿using UnityEngine;
using System.Collections;

public class follow_camera : MonoBehaviour
{



    public Transform PlayerTransform;
    private  Vector3 _cameraoffset;
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;
    public bool LookPlayer = false;
    public bool RotateAroundPlayer = true;
    public float RotationSpeed = 5.0f;


    public Vector3 distancia;

    public float horizontalSpeed = 2.0F;
    public float verticalSpeed = 2.0F;
    void Start()
    {
        _cameraoffset = transform.position + PlayerTransform.position;
    }
    void LateUpdate()
    {
        if (RotateAroundPlayer)
        {
            float h = horizontalSpeed * Input.GetAxis("Mouse X");
            float v = verticalSpeed * Input.GetAxis("Mouse Y");
            transform.Rotate(0,h, 0);
        }

        Vector3 newPos = PlayerTransform.position + _cameraoffset;
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);


        if (LookPlayer)
           transform.position= PlayerTransform.position + distancia;
    }
}
